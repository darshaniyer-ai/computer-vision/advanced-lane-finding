
# Detecting Lane Lines on the Road

When we drive, we use our eyes to decide where to go.  The lines on the road that show us where the lanes are act as our constant reference for where to steer the vehicle.  Naturally, one of the first things we would like to do in developing a self-driving car is to automatically detect lane lines using an algorithm. Previously, we developed a lane detection algorithm where the lanes were straight and solid as shown in Figure 1, and the lightning conditions were more or less uniform. Our algorithm consisted of steps that included grayscaling, gaussian smoothing, edge detection with Canny edge detector, region masking to extract region consisting primarily of lanes, applying Hough transform to the masked edges to detect line segments, and finally averaging and extrapolating the line segments. The current project is an attempt to develop an advanced lane detection algorithm where the lanes could be highly curved and broken with variable visibility brought about by highly varying lightning conditions caused by shadows, different pavement colors etc. Figures 2 and 3 are examples of challenging sets of lanes.

[image1]: ./figures/straight_lines2.jpg "Straight lanes"
![alt text][image1]
**Figure 1: Straight lanes**

[image2]: ./figures/test1.jpg "Curved lanes on yellow roads"
![alt text][image2]
**Figure 2: Curved lanes on yellow roads**

[image3]: ./figures/test4.jpg "Curved lanes with shadows"
![alt text][image3]
**Figure 3: Curved lanes with shadows**

## Overall steps 
The algorithm consists of the following steps:

* Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
* Apply a distortion correction to raw images.
* Use color transforms, gradients, etc., to create a thresholded binary image.
* Apply a perspective transform to rectify binary image ("birds-eye view").
* Detect lane pixels and fit to find the lane boundary.
* Determine the curvature of the lane and vehicle position with respect to center.
* Warp the detected lane boundaries back onto the original image.
* Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

The method works on each image from the video clip. In the end, the processed images are framed together to generate a new 
video clip with the detected lanes superimposed on each frame of the video along with their respective curvature values and the position of the vehicle with respect to the center. 

###  Camera calibration 

To steer your car, you need to measure how much your lane is curving. To do that, you need to map out the lanes in your camera images, after transforming them to a different perspective. The first step to do that is to correct for image distortion seen in camera images for the objects on the edges that are skewed or stretched. Image distortion is caused by a camera looking at 3D objects in the real world and tranforming them into a 2D image. This transformation is rendered imperfect by the distortion that changes the shape and size of these 3D objects in the captured image. We need to undo this distortion so that we can get correct and useful information out of the images.
With lens-based camera, light rays often bend a little too much or too little at the edges of a curved lens of a camera. This creates the effect of edge distortion because of which the lines or objects appear more or less curved than they actually are, which is called radial distortion. This is the most common type of distortion. On the other hand, if the camera lens is not aligned perfectly parallel to the imaging plane where the camera film or sensor is, this makes an image look tilted, so that some of the objects appear farther away or closer than they actually are. This is called tangential distortion.
For correcting distortion in real world images caused by an imperfect camera, we take multiple pictures of known shapes such as chessboard images (regular with high contrast pattern that makes it easy to detect automatically) against a flat surface from different angles and distances. We will then be able to detect any distortion by looking at the difference between the apparent size and the shape of the squares in these images, and the size and the shape that they actually are. Then a mapping is created that maps these distorted points to undistorted points, in the form of distortion coefficients and camera matrix for transforming 3D object points to 2D image points, which can later be applied to new sets of images taken with that camera. Figure 4 shows an example of a typical distorted chessboard image used for calibration. 

[image4]: ./figures/Distorted_chessboard_image.jpg "Distored chessboard image"
![alt text][image4]
**Figure 4 Distorted chessboard image used for calibration**



###  Distortion correction 

The process of distortion correction ensures that the geometrical shape of objects is represented consistently, no matter where they appear in an image. Figure 5 shows an example of a chessboard image after distortion correction using camera matrix and distortion coefficients calculated during the calibration process.

[image5]: ./figures/Undistorted_chessboard_image.jpg "Undistored chessboard image"
![alt text][image5]
**Figure 5 Chessboard image after distortion correction**





###  Perspective transformation 

Perspective is a phenomenon by which objects appear smaller the farther away they are from a viewpoint, like a camera, and parallel lines seem to converge to a point. For example, the lane looks smaller and smaller, the farther away it gets from the camera. In real world co-ordinates x, y, and z, the greater the magnitude of an objects z-coordinate or distance from the camera, the smaller it will appear in the 2D image. 
A perspective transform uses this information to transform an image. It apparently transforms the apparent z-coordinate of the object points, which in turn changes the corresponding 2D image representation. It warps the image and effectively drags points towards or away from the camera to change the apparent perspective. It lets us change our perspective to view the same scene from different viewpoints and angles. This could be viewing a scene from the side of a camera, from below the camera, or looking down on the scene from above called birds eye view. Doing a birds eye view transform is especially helpful for road images because it will also allow us to map a car's location directly with a map, since maps display roads and scenery from a top down view. 
The process of computing perspective transform is similar to undistortion process, but instead of mapping object points to image points, we want to map points in a given image to different desired image points with a new perspective. The process entails inputting two sets of four points (since four points are enough for a linear transformation from one perspective to another) called source and destination points. For example, for the lane detection example, the source points are $(x,y)$ positions of beginning and end of left and right lanes in an undistorted image and the destination points are the corresponding $(x,y)$ positions in warped birds eye view image. The following were the source and destination points used in the perspective transformation algorithm

| Source        | Destination   | 
|:-------------:|:-------------:| 
| 585, 460      | 320, 0        | 
| 203, 720      | 320, 720      |
| 1127, 720     | 960, 720      |
| 695, 460      | 960, 0        |

The perspective transform algorithms will programmatically detect four points in an image based on egde or corner detection, and analyzing attributes like color and surrounding pixels. The perspective transform returns perspective matrix *M* and inverse perspective matrix *Minv*  that can be used to navigate between the two perspectives. For example, matrix *M* can be used to navigate from the undistorted image to birds eye view image before lane detection, where as matrix *Minv* can be used to navigate from the birds eye view image to undirtorted image after detecting the lanes. Figure 6 illustrates different steps from calibration to undistortion to perspective transformation.

[image6]: ./figures/CalibrationUndistortionWarping.jpg "Chessboard image steps"
![alt text][image6]
**Figure 6 Chessboard image processing. Top left: Distorted image, Top right: Distorted image with corner points, 
Bottom left: Undistorted image, Bottom right: Warped and undistorted image**

***In the proposed advanced lane detection algorithm, the perspective transformation is applied after binary-transforming the image using methods based on gradients and color transforms. This ensures that noisy pixels, that is the pixels unrelated to lanes are minimized in the perspective-transformed birds eye view image, which in turn makes it easy on further lane detection steps down the line.***

### Generating thresholded binary image 

#### Gradient-based thresholding: Sobel operator 
We used Canny edge detector to detect edges in an image by finding pixels that were likely to part of a line in an image. Canny method gives lots of edges present in cars, sceneries, lanes, etc. But we are interested only in lane edges, which are close to vertical. We can use gradients in a smarter way to detect steep edges that are more likely related to lanes in the first place. The Sobel operator is at the heart of the Canny edge detection algorithm. Applying the Sobel operator to an image is a way of taking the derivative of the image in the *x* or *y* direction. The examples of Sobel operators with a kernel size of 3 (implying a 3 x 3 operator in each case) look like the following:

[image7]: ./figures/sobel-operator.jpg "Sobel operator"
![alt text][image7]
**Figure 7 Sobel operator: 3x3 kernels**

This is the minimum size, but the kernel size can be any odd number. A larger kernel implies taking the gradient over a larger region of the image, or, in other words, a smoother gradient.
To understand how these operators take the derivative, you can think of overlaying either one on a 3 x 3 region of an image. If the image is flat across that region (i.e., there is little change in values across the given region), then the result (summing the element-wise product of the operator and corresponding image pixels) will be zero. If, instead, for example, you apply the *Sx* operator to a region of the image where values are rising from left to right, then the result will be positive, implying a positive derivative. Taking the gradient in the *x* direction emphasizes edges closer to vertical. Alternatively, taking the gradient in the *y* direction emphasizes edges closer to horizontal. In order to obtain binary-thresholded gradient image, the steps are 
* grayscaling
* applying Sobel operators in *x* or *y* direction
* calculating the absolute value of the gradient image
* converting the absolute value gradient image to 8-bit range (0-255), and finally
* binary thresholding the 8-bit image to pick pixels based on gradient strength. 

Figure 9 illustrate the binary-thresholded *x* and *y* gradient image of an image consisting of curved lanes (Figure 8)

[image8]: ./figures/curved-lane.jpg "Curved lane"
![alt text][image8]
**Figure 8 Curved lanes**

[image9]: ./figures/curved-lane_sobel_x.jpg "Curved lane - sobel x and sobel y output"
![alt text][image9]
**Figure 9 Curved lanes - $Sobel_{x}$ and $Sobel_{y}$ gradient image**

In order to use information from both the *x* and *y* gradient, one can calculate the magnitude or absolute value of the gradient, which is just the square root of the squares of the individual *x* and *y* gradients. Gradient magnitude is at the heart of Canny edge detection, and is why Canny works well for picking up all edges. Figure 10 illustrates an example of binary-thresholded magnitude gradient image.

[image10]: ./figures/thresh-mag-example.jpg "Magnitude gradient example"
![alt text][image10]
**Figure 10 Example of magnitude gradient**

In the case of lane lines, we're interested only in edges of a particular orientation. The direction of the gradient is simply the inverse tangent (arctangent) of the *y* gradient divided by the *x* gradient. Each pixel of the resulting image contains a value for the angle of the gradient away from horizontal in units of radians, covering a range of −π/2 to π/2. An orientation of 0 implies a vertical line and orientations of +/− π/2 imply horizontal lines. Figure 11 illustrates an example of binary-thresholded orientation gradient image.

[image11]: ./figures/thresh-grad-dir-example.jpg "Orientation gradient example"
![alt text][image11]
**Figure 11 Example of orientation gradient**

#### Color-based thresholding 

The color-based thresholding works well alongside gradient-based thresholding which relies on grayscale intensity measurements.

A color space is a specific organization of colors; color spaces provide a way to categorize colors and represent them in digital images. 

[image12]: ./figures/RGB_color_space.jpg "RGB color space"
![alt text][image12]
**Figure 12 RGB color space**

RGB is red-green-blue color space. As shown in Figure 12, one can think of this as a 3D space, in this case a cube, where any color can be represented by a 3D coordinate of R, G, and B values. For example, white has the coordinate (255, 255, 255), which has the maximum value for red, green, and blue. 

There are many other ways to represent the colors in an image besides just composed of red, green, and blue values.

There is also HSV color space (hue, saturation, and value), and HLS space (hue, lightness, and saturation). These are some of the most commonly used color spaces in image analysis.

To get some intuition about these color spaces, you can generally think of Hue as the value that represents color independent of any change in brightness. So if you imagine a basic red paint color, then add some white to it or some black to make that color lighter or darker -- the underlying color remains the same and the hue for all of these colors will be the same. On the other hand, Lightness and Value represent different ways to measure the relative lightness or darkness of a color. For example, a dark red will have a similar hue but much lower value for lightness than a light red. Saturation also plays a part in this; saturation is a measurement of colorfulness. So, as colors get lighter and closer to white, they have a lower saturation value, whereas colors that are the most intense, like a bright primary color (imagine a bright red, blue, or yellow), have a high saturation value. Figure 13 depicts the HLS and HSV color spaces.

[image13]: ./figures/HSVandHLS.jpg "HSV and HLS color space"
![alt text][image13]
**Figure 13 Left: HSV color space, Right: HLS color space**

RGB space works best with white and yellow lane pixels (R and G channels pick white and yellow lanes, while B does not), but does not work well under varying light conditions. On the other hand, HLS transform isolates the lightness L component, which varies the most under different lightning conditions, where H and S components stay consistent in shadow or excessive brightness. S channel does a fairly robust job of picking up the lane lines under very different color and contrast conditions (better than R, G, B, and L channels). It's worth noting, however, that the R channel still does rather well on the white lines, perhaps even better than the S channel. As with gradients, it's worth considering how one might combine various color thresholds to make the most robust identification of the lines.

Figure 14 depicts the application of binary thresholding based on above-mentioned gradient and color-based transforms. ***For the project video, a combination of binary thresholding based on $Sobel_{x}$ (thresh = (20,100)) gradient and S (thresh = (200,255)) channel of HLS turned out to give the best overall result. For the challenge video, a combination of binary thresholding based on S (150-255) channel of HLS, R (180-255) and G (180-255) channels of RGB, Cb (0-100) channel of YCrCb, and B (180-255) channel of LAB space gave the best results. ***

[image14]: ./figures/straight_lines1_details.jpg "Gradient and color thresholding 1"
![alt text][image14]
**Figure 14 Gradient and color thresholding. A combination of $Sobel_{x}$ and *S* channel of HLS turned out to be the best.**

Figure 15 illustrates an example of challenging case with yellow lanes, very bright illumination, and curved lanes.

[image15]: ./figures/test1_details.jpg "Gradient and color thresholding 2"
![alt text][image15]
**Figure 15 Challenging case: Gradient and color thresholding. A combination of $Sobel_{x}$ and *S* channel of HLS.**

In the advanced lane detection algorithm, the perspective transform described in the previous section is applied after binary thresholding. Figures 16 and 17 depicts different steps such as raw image, undistortion with source points superimposed, binary thresholding based on $Sobel_{x}$ gradient and *S* channel of HLS, their combination, region of interest masking, and finally warped (perspective-transformed) image consisting of birds eye view with superimposed destination points for a case with straight lanes, and for a challenging case with curved, yellow lanes under bright illumination.

[image16]: ./figures/straight_lines_steps.jpg "Straight lines steps"
![alt text][image16]
**Figure 16 Straight lane case: Undistortion, binary thresholding, and perspective transformation.**

[image17]: ./figures/test1_steps.jpg "Curved-yellow lane steps"
![alt text][image17]
**Figure 17 Curved-yellow lane case: Undistortion, binary thresholding, and perspective transformation.**

### Finding the lane lines 

After applying calibration, thresholding, and a perspective transform to a road image, we have a binary image where the lane lines stand out clearly. However, we still need to decide explicitly which pixels are part of the lane lines; in other words, which pixels belong to the left lane and which ones belong to the right lane.

#### Peaks in a histogram 

As shown in Figure 18, we first take a histogram along all the columns in the lower half of the image. 

[image18]: ./figures/PixelHistogram.jpg "Pixel histogram"
![alt text][image18]
**Figure 18 Pixel histogram of the binary-thresholded birds eye view image. The histogram is computed using pixel intensities along all the columns in the lower half of the image.**

With this histogram, we are adding up the pixel values along each column in the image. Since our image is binary-thresholded, pixels are either 0 or 1, so the two most prominent peaks in this histogram will be good indicators of the x-position of the base of the lane lines. We can use that as a starting point for where to search for the lines. 

#### Sliding window 

From that point, we can use a sliding window, placed around the line centers, to find and follow the lines up to the top of the frame. The histogram is divided into two halves, and within each half the histogram peak is detected. These base peaks serve as a starting point along with a search margin of size 100 on each side of the peak, within which nonzero pixels are identified.
The sliding window is applied vertically in a non-overlapping fashion. For example, we set the window height to be 80 pixels. Since the height of the image is 720 pixels, this will result in 9 windows for each left and right sides. The nonzero pixels are identified within this window of height 80 pixels and width 200 pixels on each side, and appended to a list. If the number of nonzero pixels is greater than 50, the base peaks are updated with the mean value of the indices of the latest sets of nonzero pixels. The above steps are repeated for subsequent vertical windows. In the end, we will have two sets of lists containing the locations of nonzero pixels that are related to the lanes. 

#### Line fit 

The *x* and *y* pixels locations are extracted for the left and right lanes by indexing into all the nonzero pixels using the above-mentioned lists. A second degree polynomial fit is applied for each pair of *x* and *y* pixels positions, resulting in separate line fit for left and right lanes, as shown in Figure 19.

[image19]: ./figures/LineFitExample.jpg "Line fit using sliding window method"
![alt text][image19]
**Figure 19 Line fit using sliding window method.**

#### Subsequent frames 

Once we know where the lines are and we are confident in our fits, in the subsequent frame of video, we don't need to do a blind search again, but instead we can just search in a margin around the previous line position. 

[image20]: ./figures/SubsequentFrame.jpg "Line fit in subsequent frame"
![alt text][image20]
**Figure 20 Line fit in a subsequent frame.**

In Figure 20, the green shaded area shows where we searched for the lines this time. So, once we know where the lines are in one frame of video, we can do a highly targeted search for them in the next frame. This is equivalent to using a customized region of interest for each frame of video, and should help us track the lanes through sharp curves and tricky conditions. If we lose track of the lines, we can go back to our sliding windows search or other method to rediscover them. 

***In the proposed implementation, fit coefficients are calculated only once using the histogram-based method, for the first frame. After that we calculate fit coefficients using the targeted search mentioned above. We store the fit coefficients in a ring buffer and calculate robust average of the last thirty one fit coefficients. These average fit coefficients are used to detect the lane line positions. We noticed that this resulted in overall smooth lane boundaries.***




### Calculating the radius of curvature and vehicle position 

So far, we have a thresholded image, where we've estimated which pixels belong to the left and right lane lines (shown in red and blue, respectively, below in Figure 21), and we've fit a polynomial to those pixel positions. Now, we'll compute the radius of curvature of the fit, and the vehicle position.

#### Radius of curvature 

[image21]: ./figures/color-fit-lines.jpg "Line fit with equation"
![alt text][image21]
**Figure 21 Line fit with equation.**

We located the lane line pixels, used their x and y pixel positions to fit a second order polynomial curve:

$$f(y)=Ay^2 + By + c$$

We're fitting for *f(y)*, rather than *f(x)*, because the lane lines in the warped image are near vertical and may have the same *x* value for more than one *y* value.

The radius of curvature at any point *x* of the function *x = f(y)* is given as follows:

$$\LARGE R_{curve} = \frac{[1 + (\frac{dx}{dy})^2]^{3/2}}{|\frac{d^2x}{dy^2}|}$$

In the case of the second order polynomial above, the first and second derivatives are:

$$\large f'(y) = \frac{dx}{dy} = 2Ay + B$$

$$\large f''(y) = \frac{d^2x}{dy^2} = 2A$$

So, our equation for radius of curvature becomes:

$$\LARGE R_{curve} = \frac{(1 + (2Ay + B)^2)^{3/2}}{\left |2A \right |}$$

The *y* values of our image increase from top to bottom, so if, for example, we wanted to measure the radius of curvature closest to our vehicle, we could evaluate the formula above at the *y* value corresponding to the bottom of our image.

The radius of curvature is calculated for each left and right lane fits.

We have calculated the radius of curvature based on pixel values, so the radius we are reporting is in pixel space, which is not the same as real world space. So we actually need to repeat this calculation after converting our *x* and *y* values to real world space.

This involves measuring how long and wide the section of lane is that we're projecting in our warped image. We could do this in detail by measuring out the physical lane in the field of view of the camera, but for this project, we can assume that if we're projecting a section of lane similar to the images above, the lane is about 30 meters long and 3.7 meters wide.

The line fits are calculated by scaling the *x* and *y* pixels positions with the following factors

$$\frac{y_{m}}{pix} = \frac{30}{720}$$

$$\frac{x_{m}}{pix} = \frac{3.7}{700}$$

After that, the curvature is calculated using

$$\LARGE R_{curve} = \frac{(1 + (2Ay.\frac{y_{m}}{pix} + B)^2)^{3/2}}{\left |2A \right |}$$

#### Vehicle position 

Vehicle position is calculated with respect to the center of the lane. We assume the camera is mounted at the center of the car, such that the lane center is the midpoint at the bottom of the image between the two lines we've detected. The offset of the lane center from the center of the image (converted from pixels to meters) is the distance from the center of the lane. 

We first calculate the midpoint at the bottom of the image as the image center. The lane center is the midpoint between the two line fits at the bottom of the image. Offset is calculated as the difference between the image center and the lane center. This offset is converted from pixel space to real world space by scaling it with $\frac{x_{m}}{pix}$.


### Warping the detected lane boundaries 

The detected lane boundaries are warped onto the original image. The birds eye view image along with the line fits is warped into the original undistored image space using the inverse perspective transform matrix *Minv*. The undistorted image overlayed with detected lanes is depicted in Figure 22.

[image22]: ./figures/lane-drawn.jpg "Undistorted image with lanes"
![alt text][image22]
**Figure 22 Image with lanes drawn.**

Finally, the curvature values and the vehicle position are depicted on the above image.

Figures 23 and 24 summarize all the above described steps for case with straight lanes and a more challenging case with curved lanes and challenging lightning conditions.

[image23]: ./figures/straight_lines1_all_steps.jpg "Simpler case: Straight lanes - all the steps"
![alt text][image23]
**Figure 23 Simpler case: Straight lanes - all the steps.**

[image24]: ./figures/test1_all_steps.jpg "Challenging case: curved lanes - all the steps"
![alt text][image24]
**Figure 24 Challenging case: curved lanes - all the steps.**




```python
# <video controls src="videos/project_video_withDetectedLanes.mp4" />
```


```python
# <video controls src="videos/challenge_video_withDetectedLanes.mp4" />
```

## Shortcomings of the proposed approach

The method does not work well for extremely challenging lightning conditions.

## Possible improvements

The method must be made robust to lighting conditions by applying a transform such as homomorphic filtering which is robust to illumination changes. Additional transformation space needs to be explored where the lanes can be seen in extremely challenging conditions.

### Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to David A Ventimiglia for his blog post that gave insight on using a deque as a ring buffer. 

