# Detecting Lane Lines on the Road

When we drive, we use our eyes to decide where to go.  The lines on the road that show us where the lanes are act as our constant reference for where to steer the vehicle.  Naturally, one of the first things we would like to do in developing a self-driving car is to automatically detect lane lines using an algorithm. Previously, we developed a lane detection algorithm where the lanes were straight and solid as shown in Figure 1, and the lightning conditions were more or less uniform. Our algorithm consisted of steps that included grayscaling, gaussian smoothing, edge detection with Canny edge detector, region masking to extract region consisting primarily of lanes, applying Hough transform to the masked edges to detect line segments, and finally averaging and extrapolating the line segments. The current project is an attempt to develop an advanced lane detection algorithm where the lanes could be highly curved and broken with variable visibility brought about by highly varying lightning conditions caused by shadows, different pavement colors etc. Figures 2 and 3 are examples of challenging sets of lanes.

[image1]: ./figures/straight_lines2.jpg "Straight lanes"
![alt text][image1]
**Figure 1: Straight lanes**

[image2]: ./figures/test1.jpg "Curved lanes on yellow roads"
![alt text][image2]
**Figure 2: Curved lanes on yellow roads**

[image3]: ./figures/test4.jpg "Curved lanes with shadows"
![alt text][image3]
**Figure 3: Curved lanes with shadows**

## Overall steps
The algorithm consists of the following steps:

* Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
* Apply a distortion correction to raw images.
* Use color transforms, gradients, etc., to create a thresholded binary image.
* Apply a perspective transform to rectify binary image ("birds-eye view").
* Detect lane pixels and fit to find the lane boundary.
* Determine the curvature of the lane and vehicle position with respect to center.
* Warp the detected lane boundaries back onto the original image.
* Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

The method works on each image from the video clip. In the end, the processed images are framed together to generate a new 
video clip with the detected lanes superimposed on each frame of the video along with their respective curvature values and the position of the vehicle with respect to the center. 

## Repository 

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Advanced-Lane-Lines).


## Dependencies

This project requires:

* [CarND Term1 Starter Kit](https://github.com/udacity/CarND-Term1-Starter-Kit)

The lab environment can be created with CarND Term1 Starter Kit. Click [here](https://github.com/udacity/CarND-Term1-Starter-Kit/blob/master/README.md) for the details.

## Detailed Writeup

Detailed report can be found in [_AdvancedLaneFinding_writeup.md_](./AdvancedLaneFinding_writeup.md).

## Solution video

![](./videos/project_video_withDetectedLanes.mp4)

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to David A Ventimiglia for his blog post that gave insight on using a deque as a ring buffer. 

